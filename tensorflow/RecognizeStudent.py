import cv2
from Pack.align_custom import AlignCustom
from Pack.face_feature import FaceFeature
from Pack.mtcnn_detect import MTCNNDetect
from Pack.tf_graph import FaceRecGraph
import sys
import json
import numpy as np
import datetime
import time
import csv
import pandas as pd

def FindStudent(features_arr, positions, thres = 0.6, percent_thres = 70):
    f = open('./facerec_128D.txt','r')
    data_set = json.loads(f.read());
    returnRes = [];
    for (i,features_128D) in enumerate(features_arr):
        result = "Unknown";
        smallest = sys.maxsize
        for person in data_set.keys():
            person_data = data_set[person][positions[i]];
            for data in person_data:
                distance = np.sqrt(np.sum(np.square(data-features_128D)))
                if(distance < smallest):
                    smallest = distance;
                    result = person;
        percentage =  min(100, 100 * thres / smallest)
        if percentage <= percent_thres :
            result = "Unknown"
        returnRes.append((result,percentage))
    return returnRes


def RecognizeStudent():
    print("Camera Starting...")
    url='http://192.168.137.186:8080/video'
    vs = cv2.VideoCapture(0); #get input from webcam
    Att = []
    col_names =  ['Sno','Student Id','Date','Time']
    attendance = pd.DataFrame(columns = col_names)
    while True:
        _,frame = vs.read();
        #u can certainly add a roi here but for the sake of a demo i'll just leave it as simple as this
        rects, landmarks = face_detect.detect_face(frame,80);#min face size is set to 80x80
        aligns = []
        positions = []
        for (i, rect) in enumerate(rects):
            aligned_face, face_pos = aligner.align(160,frame,landmarks[i])
            if len(aligned_face) == 160 and len(aligned_face[0]) == 160:
                aligns.append(aligned_face)
                positions.append(face_pos)
            else: 
                print("Align face failed") #log        
        if(len(aligns) > 0):
            features_arr = extract_feature.get_features(aligns)
            recog_data = FindStudent(features_arr,positions);
            for (i,rect) in enumerate(rects):
                cv2.rectangle(frame,(rect[0],rect[1]),(rect[0] + rect[2],rect[1]+rect[3]),(255,0,0)) #draw bounding box for the face
                cv2.putText(frame,recog_data[i][0]+" - "+str(recog_data[i][1])+"%",(rect[0],rect[1]),cv2.FONT_HERSHEY_SIMPLEX,1,(255,255,255),1,cv2.LINE_AA)
                if recog_data[i][0] != 'Unknown':
                    if recog_data[i][0] not in Att:
                        sno = len(Att)+1
                        Att.append(recog_data[i][0])
                        ts = time.time()      
                        date = datetime.datetime.fromtimestamp(ts).strftime('%d-%m-%Y')
                        timeStamp = datetime.datetime.fromtimestamp(ts).strftime('%H:%M:%S')
                        attendance.loc[len(attendance)] = [sno,recog_data[i][0],date,timeStamp]
                        print(recog_data[i][0])
        attendance=attendance.drop_duplicates(subset=['Sno'],keep='first')
        cv2.imshow("Student Detection",frame)
        key = cv2.waitKey(1) & 0xFF
        if key == ord("q"):
            break
    return attendance

ts = time.time()   
date = datetime.datetime.fromtimestamp(ts).strftime('%d-%m-%Y')
present = []
FRGraph = FaceRecGraph();
aligner = AlignCustom();
extract_feature = FaceFeature(FRGraph)
face_detect = MTCNNDetect(FRGraph, scale_factor=2); #scale_factor, rescales image for faster detection
Present = RecognizeStudent()
fileName="Excel\Attendance_"+date+".csv"
Present.to_csv(fileName,index=False)

print(Present)
