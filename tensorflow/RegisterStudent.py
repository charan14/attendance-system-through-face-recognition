import cv2
from Pack.align_custom import AlignCustom
from Pack.face_feature import FaceFeature
from Pack.mtcnn_detect import MTCNNDetect
from Pack.tf_graph import FaceRecGraph
import sys
import json
import numpy as np
import datetime
import time
import csv
import pandas as pd

def RegisterStudent():
    url='http://192.168.137.186:8080/video'
    vs = cv2.VideoCapture(0); #get input from webcam
    
    print("Enter Student Id:")
    new_id = input(); 
    print("Enter Student Name")
    new_name = input(); 

    f = open('./facerec_128D.txt','r');
    data_set = json.loads(f.read());
    person_imgs = {"Left" : [], "Right": [], "Center": []};
    person_features = {"Left" : [], "Right": [], "Center": []};
    print("Start turning slowly Left to Right and Top to Bottom.");
    Count = 0
    while True:
        _, frame = vs.read();
        rects, landmarks = face_detect.detect_face(frame, 80);  # min face size is set to 80x80
        for (i, rect) in enumerate(rects):
            aligned_frame, pos = aligner.align(160,frame,landmarks[i]);
            if len(aligned_frame) == 160 and len(aligned_frame[0]) == 160:
                person_imgs[pos].append(aligned_frame)
                cv2.imshow("Captured face", aligned_frame)
                Count += 1
        key = cv2.waitKey(1) & 0xFF
        if key == ord("q"):
            break
        elif Count>200:
            break

    for pos in person_imgs: #there r some exceptions here, but I'll just leave it as this to keep it simple
        person_features[pos] = [np.mean(extract_feature.get_features(person_imgs[pos]),axis=0).tolist()]
    data_set[new_id] = person_features;
    f = open('./facerec_128D.txt', 'w');
    f.write(json.dumps(data_set))
    ts = time.time()      
    date = datetime.datetime.fromtimestamp(ts).strftime('%d-%m-%Y')
    timeStamp = datetime.datetime.fromtimestamp(ts).strftime('%H:%M:%S')
    row = [new_id , new_name, date, timeStamp]
    with open('Excel\StudentDetails.csv','a+') as csvFile:
        writer = csv.writer(csvFile)
        writer.writerow(row)
    csvFile.close()

FRGraph = FaceRecGraph();
aligner = AlignCustom();
extract_feature = FaceFeature(FRGraph)
face_detect = MTCNNDetect(FRGraph, scale_factor=2); #scale_factor, rescales image for faster detection
RegisterStudent()
