import cv2,os
import csv
import numpy as np
from PIL import Image, ImageTk
import pandas as pd
import datetime
import time
 
def is_number(s):
    try:
        val = int(s)
    except ValueError:
        return False
    return True

def TrainImages(std_Id,std_name):
    
    recognizer = cv2.face_LBPHFaceRecognizer.create()

    #load XMl File
    XmlPath = "haarcascade_frontalface_default.xml"
    detector =cv2.CascadeClassifier(XmlPath)
    imagePaths=[os.path.join("Image",f) for f in os.listdir("Image")] 
    
    faces=[]
    Ids=[]
    
    for imagePath in imagePaths:
        #loading the image and converting it to gray scale
        pilImage=Image.open(imagePath).convert('L')

        #Now we are converting the PIL image into numpy array
        imageNp=np.array(pilImage,'uint8')

        #getting the Id from the image
        Id=int(os.path.split(imagePath)[-1].split(".")[1])
        # extract the face from the training image sample
        faces.append(imageNp)
        Ids.append(Id)  

        os.unlink(imagePath)

    recognizer.train(faces, np.array(Ids))
    recognizer.save("Trainner.yml")

    #Print Result
    res = "Images Trained for ID : " + str(std_Id) +" Name : "+ str(std_name)
    print(res)


def TakeImages():      
    url='http://192.168.137.231:8080/video'
    #Read Student Id  
    print("Enter Student Id: (Only Integer)")
    Id = input()
    while not is_number(Id):
        print("Enter Student Id: (Only Integer)")
        Id = input()
    
    #Read Student Name
    print("Enter Student Name:(Only Alphabets)")
    name = input().replace(" ", "")
    while not name.isalpha():
        print("Enter Student Name:(Only Alphabets)")
        name = input().replace(" ", "")
    
    Student_id = Id
    Student_name = name
    
    try:
        cam = cv2.VideoCapture(0)
    except e:
        print(e)

    XmlPath = "haarcascade_frontalface_default.xml"
    detector=cv2.CascadeClassifier(XmlPath)
    print("Start turning slowly Left to Right and Top to Bottom.");
    Count=0
    while(True):
        ret, img = cam.read()
        gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
        faces = detector.detectMultiScale(gray, 1.3, 5)
        for (x,y,w,h) in faces:
            cv2.rectangle(img,(x,y),(x+w,y+h),(255,0,0),2)

            #incrementing sample number 
            Count += 1

            #saving the captured face in the dataset folder TrainingImage
            cv2.imwrite("Image\ "+name +"."+Id +'.'+ str(Count) + ".jpg", gray[y:y+h,x:x+w])
            #display the frame
            cv2.imshow('Capture Student',img)
        #wait for 100 miliseconds 
        if cv2.waitKey(100) & 0xFF == ord('q'):
            break
        # break if the sample number is morethan 100
        elif Count>200:
            break
    cam.release()
    cv2.destroyAllWindows() 
    res = "Captued "+ str(Count) +" Images for ID : " + Id +" Name : "+ name
    row = [Id , name]
    with open('Excel\StudentDetails.csv','a+') as csvFile:
        writer = csv.writer(csvFile)
        writer.writerow(row)
    csvFile.close()
    print(res)
    TrainImages(Student_id,Student_name)
    
TakeImages()

