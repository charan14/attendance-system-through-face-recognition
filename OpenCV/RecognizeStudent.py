import cv2,os
import csv
import numpy as np
from PIL import Image, ImageTk
import pandas as pd
import datetime
import time

def Recognize_Student():
    url='http://192.168.137.231:8080/video'

    recognizer = cv2.face.LBPHFaceRecognizer_create()
    recognizer.read("Trainner.yml")

    #Load XML File
    XmlPath = "haarcascade_frontalface_default.xml"
    faceCascade = cv2.CascadeClassifier(XmlPath)  

    #Load Students Data
    df=pd.read_csv("Excel\StudentDetails.csv")

    try:
        cam = cv2.VideoCapture(0)
    except e:
        print(e)

    font = cv2.FONT_HERSHEY_SIMPLEX        
    col_names =  ['Id','Name','Date','Time']
    attendance = pd.DataFrame(columns = col_names)    
    while True:
        ret, im =cam.read()
        gray=cv2.cvtColor(im,cv2.COLOR_BGR2GRAY)
        faces=faceCascade.detectMultiScale(gray, 1.2,5)  

        for(x,y,w,h) in faces:
            cv2.rectangle(im,(x,y),(x+w,y+h),(225,0,0),2)
            Id, conf = recognizer.predict(gray[y:y+h,x:x+w])  


            tt = ''

            # print(conf)
            if(conf < 50):
                ts = time.time()      
                date = datetime.datetime.fromtimestamp(ts).strftime('%d-%m-%Y')
                timeStamp = datetime.datetime.fromtimestamp(ts).strftime('%H:%M:%S')
                aa=df.loc[df['Id'] == Id]['Name'].values[0]
                tt=aa
                attendance.loc[len(attendance)] = [Id,aa,date,timeStamp]
                       
            cv2.putText(im,str(tt),(x,y+h), font, 1,(255,255,255),2)        
        attendance=attendance.drop_duplicates(subset=['Id'],keep='first')    
        cv2.imshow('Recognize Student',im) 

        if (cv2.waitKey(1)==ord('q')):
            break

    ts = time.time()      
    date = datetime.datetime.fromtimestamp(ts).strftime('%Y-%m-%d')
    timeStamp = datetime.datetime.fromtimestamp(ts).strftime('%H:%M:%S')
    Hour,Minute,Second=timeStamp.split(":")

    fileName="Excel\Attendance_"+date+".csv"
    attendance.to_csv(fileName,index=False)
    cam.release()
    cv2.destroyAllWindows()
    print(attendance)

Recognize_Student()